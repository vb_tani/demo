# 社内Git説明用リポジトリ

Bitbucket で初めてpublicリポジトリ作りました！  
社内説明用に使うリポジトリです。  

# 説明内容

## Gitの基本操作
 - git の３状態
 - git clone 
 - git add
 - git commit
 - git push
 - 別PCでのgit pull

## Gitのブランチ
 - ブランチを作成
 - ブランチを削除
 - ブランチ切替
 - ブランチをマージ

## 勉強になるサイト様
サルでもわかるGit入門  
https://backlog.com/git-tutorial/ja/  

Pro Git  
https://git-scm.com/book/ja/v2  
